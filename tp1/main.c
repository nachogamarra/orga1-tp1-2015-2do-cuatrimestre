#include "lista.h"
#include <stdio.h>
#include <stdlib.h>

void ej1();
void ej2();
void ej3();
void ej4();
void ej5();
void ej6();
void ej7();
void ej8();
void ej9();
void ej10();
void ej11();
void ej12();
void ej13();

void g(char *s){
   if( s[0] != 0 ) s[0] = 'X';
}


int main (void){
	ej1();
	ej2();
	ej3();
	ej4();
	ej5();
	ej6();
	ej7();
	ej8();
	ej9();
	ej10();
	ej11();
	ej12();
	ej13();

	return 0;
}

void ej1() {
	printf("la longitud de ’hola’ es = %d \n", palabraLongitud("hola"));
}

void ej2() {	
	if (palabraMenor("merced", "mercurio")) 
		printf ("TRUE\n"); else printf ("FALSE\n");	
	if (palabraMenor("perro", "zorro")) 
		printf ("TRUE\n"); else printf ("FALSE\n");
	if (palabraMenor("senior", "seniora")) 
		printf ("TRUE\n"); else printf ("FALSE\n");
	if (palabraMenor("caZa", "casa")) 
		printf ("TRUE\n"); else printf ("FALSE\n");
	if (palabraMenor("hola", "hola")) 
		printf ("TRUE\n"); else printf ("FALSE\n");
}

void ej3() {
	char unaPalabra[] = "hola";
	palabraFormatear(unaPalabra, g);
}

void ej4() {
	char unaPalabra[] = "hola";
	palabraImprimir(unaPalabra,  stdout);
}

void ej5() {
	char *unaPalabra = palabraCopiar("hola");
	char *otraPalabra = palabraCopiar(unaPalabra);
	unaPalabra[1] = 'X';
	palabraImprimir(unaPalabra, stdout);
	palabraImprimir(otraPalabra, stdout);
	free(unaPalabra);
	free(otraPalabra);
}

void ej6() {
	nodo *miNodo = nodoCrear(palabraCopiar("algunaPalabra"));
	printf( "Palabra del Nodo: %s\n", miNodo->palabra );
	nodoBorrar( miNodo );
}

void ej7() {
	lista *miLista = oracionCrear();
	oracionImprimir( miLista, "salida.txt", palabraImprimir );
	oracionBorrar( miLista );
}

void ej8() {
	lista *miLista = oracionCrear();
	char *palabra1 = palabraCopiar( "palabra1" );
	char *palabra2 = palabraCopiar( "palabra2" );
	char *palabra3 = palabraCopiar( "palabra3" );
	insertarAtras( miLista, palabra1 );
	oracionImprimir( miLista, "salida.txt", palabraImprimir );
	insertarAtras( miLista, palabra2 );
	insertarAtras( miLista, palabra3 );
	oracionImprimir( miLista, "salida.txt", palabraImprimir );
	oracionBorrar( miLista );
}

void ej9() {
	lista *miLista = oracionCrear();
	char *palabra1 = palabraCopiar( "Un" );
	char *palabra2 = palabraCopiar( "Dos" );
	char *palabra3 = palabraCopiar( "Tres" );
	char *palabra4 = palabraCopiar( "Cuatro" );
	insertarAtras( miLista, palabra1 );
	printf( "LongMedia = %2.5f\n", longitudMedia( miLista ) );
	insertarAtras( miLista, palabra2 );
	printf( "LongMedia = %2.5f\n", longitudMedia( miLista ) );
	insertarAtras( miLista, palabra3 );
	printf( "LongMedia = %2.5f\n", longitudMedia( miLista ) );
	insertarAtras( miLista, palabra4 );
	printf( "LongMedia = %2.5f\n", longitudMedia( miLista ) );
	oracionBorrar( miLista );
}

void ej10() {
	lista *miLista = oracionCrear();
	insertarOrdenado( miLista, palabraCopiar( "h" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "m" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "t" ),palabraMenor );
//	oracionImprimir( miLista, "salida.txt", palabraImprimir );

	insertarOrdenado( miLista, palabraCopiar( "b" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "c" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "a" ),palabraMenor );
//	oracionImprimir( miLista, "salida.txt", palabraImprimir );

	insertarOrdenado( miLista, palabraCopiar( "d" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "f" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "e" ),palabraMenor );
//	oracionImprimir( miLista, "salida.txt", palabraImprimir );

	insertarOrdenado( miLista, palabraCopiar( "i" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "j" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "g" ),palabraMenor );
//	oracionImprimir( miLista, "salida.txt", palabraImprimir );

	insertarOrdenado( miLista, palabraCopiar( "k" ),palabraMenor );
//	oracionImprimir( miLista, "salida.txt", palabraImprimir );

	insertarOrdenado( miLista, palabraCopiar( "o" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "n" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "p" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "q" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "l" ),palabraMenor );
//	oracionImprimir( miLista, "salida.txt", palabraImprimir );

	insertarOrdenado( miLista, palabraCopiar( "s" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "r" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "v" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "u" ),palabraMenor );
	oracionImprimir( miLista, "salida.txt", palabraImprimir );
	
	
	
	
	oracionBorrar(miLista);
}

void ej11() {

}

void ej12() {
	lista *miLista = oracionCrear();
	insertarOrdenado( miLista, palabraCopiar( "palabra1" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "palabra2" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "palabra3" ),palabraMenor );
	
	filtrarPalabra( miLista, palabraMenor, "palabra3" );
	oracionImprimir( miLista, "salida.txt", palabraImprimir);
	filtrarPalabra( miLista, palabraIgual, "palabra1" );
	oracionImprimir( miLista, "salida.txt", palabraImprimir);
	oracionBorrar(miLista);

	lista *miLista2 = oracionCrear();
	oracionImprimir( miLista2, "salida.txt", palabraImprimir);
	insertarOrdenado( miLista2, palabraCopiar( "h" ),palabraMenor );
	insertarOrdenado( miLista2, palabraCopiar( "m" ),palabraMenor );
	insertarOrdenado( miLista2, palabraCopiar( "t" ),palabraMenor );

	insertarOrdenado( miLista2, palabraCopiar( "b" ),palabraMenor );
	insertarOrdenado( miLista2, palabraCopiar( "c" ),palabraMenor );
	insertarOrdenado( miLista2, palabraCopiar( "a" ),palabraMenor );

	insertarOrdenado( miLista2, palabraCopiar( "d" ),palabraMenor );
	insertarOrdenado( miLista2, palabraCopiar( "f" ),palabraMenor );
	insertarOrdenado( miLista2, palabraCopiar( "e" ),palabraMenor );

	insertarOrdenado( miLista2, palabraCopiar( "i" ),palabraMenor );
	insertarOrdenado( miLista2, palabraCopiar( "j" ),palabraMenor );
	insertarOrdenado( miLista2, palabraCopiar( "g" ),palabraMenor );

	insertarOrdenado( miLista2, palabraCopiar( "k" ),palabraMenor );

	insertarOrdenado( miLista2, palabraCopiar( "o" ),palabraMenor );
	insertarOrdenado( miLista2, palabraCopiar( "n" ),palabraMenor );
	insertarOrdenado( miLista2, palabraCopiar( "p" ),palabraMenor );
	insertarOrdenado( miLista2, palabraCopiar( "q" ),palabraMenor );
	insertarOrdenado( miLista2, palabraCopiar( "l" ),palabraMenor );

	insertarOrdenado( miLista2, palabraCopiar( "s" ),palabraMenor );
	insertarOrdenado( miLista2, palabraCopiar( "r" ),palabraMenor );
	insertarOrdenado( miLista2, palabraCopiar( "v" ),palabraMenor );
	insertarOrdenado( miLista2, palabraCopiar( "u" ),palabraMenor );

	filtrarPalabra( miLista2, palabraIgual, "z" );
	filtrarPalabra( miLista2, palabraIgual, "a" );
	filtrarPalabra( miLista2, palabraIgual, "i" );
	filtrarPalabra( miLista2, palabraIgual, "u" );
	filtrarPalabra( miLista2, palabraIgual, "e" );
	filtrarPalabra( miLista2, palabraIgual, "o" );

	oracionImprimir( miLista2, "salida.txt", palabraImprimir);
	oracionBorrar(miLista2);

	lista *miLista3 = oracionCrear();
	insertarOrdenado( miLista3, palabraCopiar( "1" ),palabraMenor );
	insertarOrdenado( miLista3, palabraCopiar( "3" ),palabraMenor );
	insertarOrdenado( miLista3, palabraCopiar( "2" ),palabraMenor );
	
	filtrarPalabra( miLista3, palabraMenor, "4" );
	oracionImprimir( miLista3, "salida.txt", palabraImprimir);
	filtrarPalabra( miLista3, palabraIgual, "4" );
	oracionImprimir( miLista3, "salida.txt", palabraImprimir);
	oracionBorrar(miLista3);

	lista *miLista4 = oracionCrear();
	insertarOrdenado( miLista4, palabraCopiar( "hola" ),palabraMenor );
	oracionImprimir( miLista4, "salida.txt", palabraImprimir);
	filtrarPalabra( miLista4, palabraIgual, "quilo" );
	oracionImprimir( miLista4, "salida.txt", palabraImprimir);
	oracionBorrar(miLista4);


}

void ej13() {
	lista *miLista = oracionCrear();
	descifrarMensajeDiabolico( miLista, "salida.txt", palabraImprimir );
	insertarOrdenado( miLista, palabraCopiar( "h" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "m" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "t" ),palabraMenor );

	insertarOrdenado( miLista, palabraCopiar( "b" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "c" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "a" ),palabraMenor );

	insertarOrdenado( miLista, palabraCopiar( "d" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "f" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "e" ),palabraMenor );

	insertarOrdenado( miLista, palabraCopiar( "i" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "j" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "g" ),palabraMenor );

	insertarOrdenado( miLista, palabraCopiar( "k" ),palabraMenor );

	insertarOrdenado( miLista, palabraCopiar( "o" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "n" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "p" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "q" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "l" ),palabraMenor );

	insertarOrdenado( miLista, palabraCopiar( "s" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "r" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "v" ),palabraMenor );
	insertarOrdenado( miLista, palabraCopiar( "u" ),palabraMenor );

	descifrarMensajeDiabolico( miLista, "salida.txt", palabraImprimir );
	oracionBorrar(miLista);
}

; PALABRA
	global palabraLongitud
	global palabraMenor
	global palabraFormatear
	global palabraImprimir
	global palabraCopiar
	
; LISTA y NODO
	global nodoCrear
	global nodoBorrar
	global oracionCrear
	global oracionBorrar
	global oracionImprimir

; AVANZADAS
	global longitudMedia
	global insertarOrdenado
	global filtrarPalabra
	global descifrarMensajeDiabolico

; YA IMPLEMENTADAS EN C
	extern palabraIgual
	extern insertarAtras

; BIBLIOTECAS DE C
	extern fprintf, fopen, fclose, malloc, free

; /** DEFINES **/    >> SE RECOMIENDA COMPLETAR LOS DEFINES CON LOS VALORES CORRECTOS
	%define NULL 		0
	%define TRUE 		1
	%define FALSE 		0

	%define LISTA_SIZE 	    	 8
	%define OFFSET_PRIMERO 		 0

	%define NODO_SIZE     		 16
	%define OFFSET_SIGUIENTE   	 0
	%define OFFSET_PALABRA 		 8

	append:			 		db 'a',0		;MODO APPEND
	format_string_con_LF: 	db '%s', 10, 0
	oracionVacia:			db '<oracionVacia>', 0
	sinMensajeDiabolico:	db '<sinMensajeDiabolico>', 0

section .rodata


section .data


section .text


;/** FUNCIONES DE PALABRAS **/
;-----------------------------------------------------------

	; unsigned char palabraLongitud( char *p ) {
	;   unsigned char res = 0;
	;   char *it = palabra;
	;   while (*it != 0) {
	;     res++;
	;     it++;
	;   }
	;   return res;
	; }
	palabraLongitud:
		;STACKFRAME
						;DESALINEADA
		push rbp		;ALINEADA
		mov rbp, rsp

		;CODIGO

		xor rax, rax	; res = 0;
		mov r10, rdi	; it = palabra;

		; while (*it != 0) 
		.contar_caracteres:
		cmp byte [r10], 0
			je .fin
			inc rax		; res++;
			inc r10		; it++;
			jmp .contar_caracteres

		.fin:
			;FIN
			pop rbp
			ret

	; bool palabraMenor( char *p1, char *p2 ) {
	;   char *it1 = p1;
	;   char *it2 = p2;
	;   while ((*it1 != 0) || (*it2 != 0))    // comparo ambos strings caracter a caracter
	;   {
	;     if (*it1 < *it2) return true;       // es menor
	;     if (*it1 > *it2) return false;      // es mayor
	;     it1++;
	;     it2++;
	;   }
	;   if (*it1 == 0 &&  *it2 != 0) return true;    // es menor
	;   if (*it1 != 0 &&  *it2 == 0) return false;    // es mayor
	;   return false;                             // son iguales
	; }
	palabraMenor:
		;STACKFRAME
						;DESALINEADA
		push rbp		;ALINEADA
		mov rbp, rsp
		push r12		;DESALINEADA
		push r13		;ALINEADA

	; 	;CODIGO

	 	mov r12, rdi	; it1 = p1	
	 	mov r13, rsi	; it2 = p2
	 	xor rax, rax	; res = false

	 	; while ((*it1 != 0) || (*it2 != 0))
	 	.recorrer_string:
			cmp byte [r12], 0
			je .fin_palabra_1
			cmp byte [r13], 0
			je .fin_palabra_2
	 		mov byte r8b, [r12]
	 		mov byte r9b, [r13]
	 		cmp r8b, r9b
	 		jl .devolver_true	; if (*it1 < *it2) return true;       // es menor
	 		jg .devolver_false	; if (*it1 > *it2) return false;      // es mayor
	 		inc r12				; it1++;
	 		inc r13				; it2++;
	 		jmp .recorrer_string

	 	.fin_palabra_1:
	 	cmp byte [r13], 0
	 	je .son_iguales
	 	jmp .devolver_true
	 	.fin_palabra_2:
	 	cmp byte [r12], 0
	 	je .son_iguales
	 	jmp .devolver_false

	 	.son_iguales:
	 		mov al, FALSE
	 		jmp .fin
	 	.devolver_false:
			mov al, FALSE
			jmp .fin
		.devolver_true:
			mov al, TRUE
			jmp .fin

		.fin:
			;FIN
			pop r13
			pop r12
			pop	rbp
			ret

	; void palabraFormatear( char *p, void (*funcModificarString)(char*) ) {
	;     funcModificarString(p);
	; }
	palabraFormatear:
		;STACKFRAME
						;DESALINEADA
		push rbp		;ALINEADA
		mov rbp, rsp

		;CODIGO

		; rdi: p
		; rsi: funcModificarString

		; NOTA: en RDI ya se encuentra el "p" para pasar a la funcion

					; ALINEADA
		call rsi	; funcModificarString(p);

		;FIN
		pop	rbp
		ret


	; void palabraImprimir( char *p, FILE *file ) {
	;     fprintf(file, "%s\n", p);
	; }
	palabraImprimir:
		;STACKFRAME
						;DESALINEADA
		push rbp		;ALINEADA
		mov rbp, rsp

		;CODIGO

		; rdi: p
		; rsi: file
		;int fprintf ( FILE * stream, const char * format, ... );

		mov r10, rdi	;salvo p
		; parametros: 
		; 	rdi = file = rsi
		; 	rsi = format_string_con_LF
		; 	rdx = p = r10

		mov rdi, rsi
		mov rsi, format_string_con_LF
		mov rdx, r10
		mov rax,1 			;para llamar a fprintf
		call fprintf		;ALINEADA

		;FIN
		pop	rbp
		ret
			

	; char *palabraCopiar( char *p ){
	; {
	; 	char *it = s;
	; 	int cant_letras = 0;
	; 	while (*it != 0)							// cuento las letras del string a copiar
	; 	{
	; 		it++;
	; 		cant_letras++;
	; 	}
	; 	char *res = (void *)malloc(cant_letras);	// reservo espacio para nuevo string
	; 	char *it_r = res;
	; 	char *it_s = s;
	; 	for (int i = 0; i <= cant_letras ; i++)		// copio caracter a caracter
	; 	{
	; 		*it_r = *it_s;
	; 		it_r++;
	; 		it_s++;
	; 	}
	; 	return res;
	; }
	palabraCopiar:
		;STACKFRAME
						;DESALINEADA
		push rbp		;ALINEADA
		mov rbp, rsp
		push r12		;DESALINEADA
		push r13		;ALINEADA

		;CODIGO
		mov r12, rdi	; salvo p

		; parametros
		;	rdi : p
		call palabraLongitud 	;en rax esta la cantidad de caracteres
		mov r13, rax

		.crear_palabra:
		inc r13		;cuento el caracter de escape
		mov rdi, r13
		call malloc	;ALINEADA
		mov r10, rax

		; r12: palabra a copiar
		; rax: palabra resultado
		.copiar_caracteres:
			cmp byte [r12], 0
			je .fin_caracter
			mov byte r9b, [r12]
			mov byte [r10], r9b
			inc r10
			inc r12
			jmp .copiar_caracteres

		.fin_caracter:
		mov byte [r10],0

		;FIN
		pop r13
		pop r12
		pop	rbp
		ret


;/** FUNCIONES DE LISTA Y NODO **/
;-----------------------------------------------------------

	; nodo *nodoCrear( char *palabra ) {
	; 	nodo *n = (void *) malloc(NODO_SIZE);
	; 	n->siguiente = NULL;
	; 	n->palabra = p;
	;	return n;
	; }
	nodoCrear:
		;STACKFRAME
						;DESALINEADA
		push rbp		;ALINEADA
		mov rbp, rsp
		push r12		;DESALINEADA
		sub rsp, 8		;ALINEADA
		
		;CODIGO
		mov r12, rdi

		;LLAMADA A MALLOC
		mov	rdi, NODO_SIZE	;COLOCO LA CANT. DE MEMORIA A RESERVAR
							;ALINEADA
		call malloc
							;rax <- [posicion de memoria asignada]

		mov qword [rax + OFFSET_SIGUIENTE], NULL
		mov qword [rax + OFFSET_PALABRA], r12

		;devuelvo la direccion del nuevo nodo

		.fin:
		;FIN
		add rsp, 8
		pop r12
		pop	rbp
		ret

	; void nodoBorrar( nodo *n ) {
	;	free(n->palabra);
	; 	free(n)
	; }
	nodoBorrar:
		;STACKFRAME
						;DESALINEADA
		push rbp		;ALINEADA
		mov rbp, rsp
		push r12		;DESALINEADA
		sub rsp, 8		;ALINEADA
		
		;CODIGO
			
		; NOTA: en rdi ya se encuentra la posicion a borrar
		mov r12, rdi
		mov rdi, [r12 + OFFSET_PALABRA]
		call free		; borro la palabra

		mov rdi, r12
		call free		; borro el nodo

		.fin:
			;FIN
			add rsp, 8
			pop r12
			pop	rbp
			ret

	; lista *oracionCrear( void ) {
	; 	lista *l = (void *) malloc(LISTA_SIZE);
	; 	n->primero = NULL;
	;	return l;
	; }
	oracionCrear:
		;STACKFRAME
						;DESALINEADA
		push rbp		;ALINEADA
		mov rbp, rsp

		;CODIGO

		;LLAMADA A MALLOC
		mov	rdi, LISTA_SIZE	;COLOCO LA CANT. DE MEMORIA A RESERVAR
							;ALINEADA
		call malloc
							;rax <- [posicion de memoria asignada]

		mov qword [rax + OFFSET_PRIMERO], NULL

		;devuelvo la direccion del nuevo nodo

		.fin:
			;FIN
			pop	rbp
			ret		

	; void oracionBorrar( lista *l ) {
	;     nodo* it = l->primero;
	;     nodo* it_sig;
	;     while (it != NULL) {
	;         it_sig = it->siguiente;
	;         nodoBorrar(it);
	;         it = it_sig;
	;     }
	;     free(l);
	; }
	oracionBorrar:
		;STACKFRAME
						;DESALINEADA
		push rbp		;ALINEADA
		mov rbp, rsp
		push r12		;DESALINEADA
		push r13
		push r14		;DESALINEADA
		sub rsp, 8		;ALINEADA
		
		;CODIGO
		mov r12, rdi
		cmp r12, NULL
		je .fin

		mov r13, [r12 + OFFSET_PRIMERO]	; it = l->primero;

		.borrar_nodos:
			cmp r13, NULL
			je .borrar_lista
			mov r14, [r13 + OFFSET_SIGUIENTE] ; it_sig = it->siguiente
			; nodoBorrar(it)
			mov rdi, r13
			call nodoBorrar
			mov r13, r14	; it = it_sig
			jmp .borrar_nodos

		.borrar_lista:
		mov rdi, r12
						;ALINEADA
		call free		;borro la lista

		.fin:
			;FIN
			add rsp, 8
			pop r14
			pop r13
			pop r12
			pop	rbp
			ret		

	; void oracionImprimir( lista *l, char *archivo, void (*funcImprimirPalabra)(char*,FILE*) ) {
	;     FILE *file = fopen(archivo,"a");
	;     if (l->primero == NULL) {
	;         fprintf(file, "%s\n", "<oracionVacia>");
	;    } else {
	;	     nodo* it = l->primero;
	;	     while (it != NULL) {
	;	         funcImprimirPalabra(it->palabra, file);
	;	         it = it->siguiente;
	;     	}
	;	}
	;     fclose(file);
	; }
	oracionImprimir:
		;STACKFRAME
						;DESALINEADA
		push rbp		;ALINEADA
		mov rbp, rsp
		push r12		;DESALINEADA
		push r13
		push r14		;DESALINEADA
		push r15
		push rbx		;DESALINEADA
		sub rsp, 8		;ALINEADA
		
		;CODIGO

		; salvo parametros
		mov r12, rdi	; r12 = l
		mov r13, rsi	; r13 = archivo
		mov rbx, rdx	; rbx = funcImprimirPalabra

		;FILE *file = fopen(archivo,"a");
		mov rdi, r13
		mov rsi, append
					; ALINEADA
		call fopen
		mov r13, rax

		mov qword r14, [r12 + OFFSET_PRIMERO]	;r14 iterador de la lista

		cmp r14, NULL	; if (l->primero == NULL) fprintf (archivo, "%s\n","oracionVacia");
		jne .no_esta_vacia

			mov rdi, r13			; rdi = file
			mov rsi, format_string_con_LF
			mov rdx, oracionVacia	;para imprimir '<oracionVacia>'
			mov rax,1 				;para llamar a fprintf
			call fprintf			;ALINEADA
			jmp .fin

		; else
		.no_esta_vacia:
			; nodo* it = l->primero;
			; while (it != NULL) {
			;     funcImprimirPalabra(it->palabra, file);
			;     it = it->siguiente;
			; }
			cmp r14, NULL
				je .fin
				; funcImprimirPalabra(it->palabra, file)
				mov qword rdi, [r14 + OFFSET_PALABRA]
				mov rsi, r13
							; ALINEADA
				call rbx				
				mov qword r14, [r14 + OFFSET_SIGUIENTE]
				jmp .no_esta_vacia

		.fin:
			; fclose(file)
			mov rdi, r13
			call fclose
			;FIN
			add rsp, 8
			pop rbx
			pop r15
			pop r14
			pop r13
			pop r12
			pop	rbp
			ret


;/** FUNCIONES AVANZADAS **/
;-----------------------------------------------------------

	; float longitudMedia( lista *l ) {
	;     float res = 0;
	;     int suma = 0;
	;     int longitud_lista = 0;
	;     nodo* it = l->primero;
	;     while (it != NULL) {
	;         suma += palabraLongitud(it->palabra);
	;         longitud_lista++;
	;         it = it->siguiente;
	;     }
	;     if (longitud_lista != 0) 
	;       res = ((float)suma/(float)longitud_lista);
	;     return res;
	; }
	longitudMedia:
		;STACKFRAME
						;DESALINEADA
		push rbp		;ALINEADA
		mov rbp, rsp
		push r12		;DESALINEADA
		push r13		;ALINEADA
		push r14		;DESALINEADA
		sub rsp, 8		;ALINEADA
		
		;CODIGO
		xor r12, r12			; suma = 0
		xor r13, r13			; longitud_lista = 0

		mov qword r14, [rdi + OFFSET_PRIMERO]	; it = l->primero
		; while (it != NULL) {
		;     suma += palabraLongitud(it->palabra);
		;     longitud_lista++;
		;     it = it->siguiente;
		; }
		.sumar_longitudes:
			cmp r14, NULL
			je .promediar

			; suma += palabraLongitud(it-palabra)
			mov rdi, [r14 + OFFSET_PALABRA] 
			call palabraLongitud
			add r12, rax

			inc r13								; longitud_lista++
			mov r14, [r14 + OFFSET_SIGUIENTE]	; it = it->siguiente;
			jmp .sumar_longitudes

		.promediar:
			xorpd xmm0, xmm0	; res = 0
			cmp r13, 0			; para evitar dividir por cero
			je .fin

			xorpd xmm2, xmm2
			cvtsi2ss xmm0, r12
			cvtsi2ss xmm2, r13
			divss xmm0, xmm2

			;resultado en xmm0
			
			
		.fin:
			;FIN
			add rsp, 8
			pop r14
			pop r13
			pop r12
			pop	rbp
			ret

	; void insertarOrdenado( lista *l, char *palabra, bool (*funcCompararPalabra)(char*,char*) ) {
	;     if (l->primero == NULL) // si la lista estaba vacía, va al final
	;     {
	;         insertarAtras(l, palabra);
	;     } else {
	;       nodo *it = l->primero;
	;       if (funcCompararPalabra(palabra, it->palabra)) { // si es el primero lo pongo al principio
	;         nodo* nodo_nuevo = nodoCrear(palabra);
	;         l->primero = nodo_nuevo;
	;         nodo_nuevo->siguiente = it;
	;       } else {
	;         nodo *nodo_anterior = it;
	;         it = it->siguiente;
	;         while ((it != NULL) && !funcCompararPalabra(palabra, it->palabra))
	;         {
	;           nodo_anterior = it;
	;           it = it->siguiente;
	;         }
	;         if (it == NULL)   // ninguno cumplió la comparación, va al final
	;         {
	;           insertarAtras(l,palabra);
	;         } else  // 
	;         {
	;           nodo* nodo_nuevo = nodoCrear(palabra);
	;           nodo_anterior->siguiente = nodo_nuevo;
	;           nodo_nuevo->siguiente = it;
	;         } 
	;       }
	;     }
	; }
	insertarOrdenado:
		;STACKFRAME
						;DESALINEADA
		push rbp		;ALINEADA
		mov rbp, rsp
		push r12		;DESALINEADA
		push r13
		push r14		;DESALINEADA
		push r15
		push rbx		;DESALINEADA
		sub rsp, 8		;ALINEADA

		;CODIGO
		; rdi = l
		; rsi = palabra
		; rdx = funcCompararPalabra

		; salvo los parametros
		mov r12, rdi
		mov r13, rsi
		mov rbx, rdx

		; r14 = iterador de la lista
		mov r14, [r12 + OFFSET_PRIMERO]

		; if (l->primero == NULL) // si la lista estaba vacía, va al final
		; {
		;     insertarAtras(l, palabra);
		; } else { ... }
		cmp r14, NULL	; r14 aun es el primero de la lista
		jne .no_estaba_vacia
			; insertarAtras(l, palabra);
			mov rdi, r12
			mov rsi, r13
			call insertarAtras
			jmp .fin

		; else
		.no_estaba_vacia:
			; if (funcCompararPalabra(palabra, it->palabra)) { // si es el primero lo pongo al principio
			;     nodo* nodo_nuevo = nodoCrear(palabra);
			;     l->primero = nodo_nuevo;
			;     nodo_nuevo->siguiente = it;
			; }	else { ... }
			mov rdi, r13
			mov qword rsi, [r14 + OFFSET_PALABRA]
			call rbx
			; en al esta el resultado de la comparacion

			cmp al, FALSE
			je .no_va_al_principio
				mov rdi, r13
				call nodoCrear
				; en rax esta la direccion del nuevo nodo
				mov qword [r12 + OFFSET_PRIMERO], rax
				mov qword [rax + OFFSET_SIGUIENTE], r14
				jmp .fin

			; else
			.no_va_al_principio:
				; nodo *nodo_anterior = it;
	   			; it = it->siguiente;
	   			mov r15, r14		; r15 = nodo_anterior
	   			mov r14, [r14 + OFFSET_SIGUIENTE]
	
	   			; while ((it != NULL) && !funcCompararPalabra(palabra, it->palabra))
	   			.recorrer_lista:
		   			cmp r14, NULL
		   			je .fin_recorrer_lista
		   			mov rdi, r13
		   			mov rsi, [r14 + OFFSET_PALABRA]
		   			call rbx
		   			cmp al, TRUE
		   			je .fin_recorrer_lista
					; nodo_anterior = it;
					; it = it->siguiente;
					mov r15, r14
					mov r14, [r14 + OFFSET_SIGUIENTE]
					jmp .recorrer_lista
	   			
	   			.fin_recorrer_lista:
			        ; if (it == NULL)   // ninguno cumplió la comparación, va al final
			        ; {
			        ;   insertarAtras(l,palabra);
			        ; } else { ... } 
			        cmp r14, NULL
			        jne .insertar_al_medio
			        mov rdi, r12
			        mov rsi, r13
			        call insertarAtras
			        jmp .fin

			    ; else // va al medio
			    .insertar_al_medio:
			        ; nodo* nodo_nuevo = nodoCrear(palabra);
			        ; nodo_anterior->siguiente = nodo_nuevo;
			        ; nodo_nuevo->siguiente = it;
			        mov rdi, r13
			        call nodoCrear
			        ; en rax esta la direccion del nuevo nodo
			        mov [r15 + OFFSET_SIGUIENTE], rax
			        mov [rax + OFFSET_SIGUIENTE], r14

		.fin:
			;FIN
			add rsp, 8
			pop rbx
			pop r15
			pop r14
			pop r13
			pop r12
			pop	rbp
			ret


	; void filtrarPalabra( lista *l, bool (*funcCompararPalabra)(char*,char*), char *palabraCmp );
    ; nodo *it_anterior = (nodo *)l;
    ; nodo *it = l->primero;
    ; while (it != NULL) {
    ;     if (!funcCompararPalabra(it->palabra, palabraCmp)) {
    ;         it_anterior->siguiente = it->siguiente;
    ;         if (l->primero == it) {
    ;             l->primero = it->siguiente;
    ;         }
    ;         nodoBorrar(it);
    ;         it = it_anterior->siguiente;
    ;     } else {
    ;         it_anterior = it;
    ;         it = it->siguiente;
    ;     }
    ; }
	filtrarPalabra:
		;STACKFRAME
						;DESALINEADA
		push rbp		;ALINEADA
		mov rbp, rsp
		push r12		;DESALINEADA
		push r13
		push r14		;DESALINEADA
		push r15
		push rbx		;DESALINEADA
		sub rsp, 8		;ALINEADA

		;CODIGO
		; rdi = l
		; rsi = funcCompararPalabra
		; rdx = palabraCmp

		; salvo los parametros
		mov r12, rdi
		mov r13, rsi
		mov rbx, rdx

	    ; nodo *it_anterior = (nodo *)l;
	    ; nodo *it = l->primero;
	    mov r15, r12					; r15 = it_anterior
	    mov r14, [r12 + OFFSET_PRIMERO]	; r14 = it

	    ; while (it != NULL) { ... } 
	    .recorrer_lista:
		    cmp r14, NULL
		    je .fin_recorrer_lista
			; if (!funcCompararPalabra(it->palabra, palabraCmp)) {
			;     it_anterior->siguiente = it->siguiente;
			;     ...
			; } else { ... }
			mov rdi, [r14 + OFFSET_PALABRA]
			mov rsi, rbx
			call r13
			; en al esta el resultado de la comparacion
			cmp al, FALSE
				jne .sigo_recorriendo
				; it_anterior->siguiente = it->siguiente;
				; uso r9 de auxiliar porque no puedo hacer MOV [X], [Y]
				mov r9, [r14 + OFFSET_SIGUIENTE]
				mov [r15 + OFFSET_SIGUIENTE], r9

				; if (l->primero == it) {
				;     l->primero = it->siguiente;
				; }
				cmp [r12 + OFFSET_PRIMERO], r14
					jne .no_es_el_primero_de_la_lista
					mov r9, [r14 + OFFSET_SIGUIENTE]
					mov [r12 + OFFSET_PRIMERO], r9

				.no_es_el_primero_de_la_lista:
				; nodoBorrar(it);
				; it = it_anterior->siguiente;
				mov rdi, r14
				call nodoBorrar
				mov r14, [r15 + OFFSET_SIGUIENTE]
				jmp .recorrer_lista

			; else
			.sigo_recorriendo: 
			; it_anterior = it;
			; it = it->siguiente;
			mov r15, r14
			mov r14, [r14 + OFFSET_SIGUIENTE]
			jmp .recorrer_lista

		.fin_recorrer_lista:

		.fin:
			;FIN
			add rsp, 8
			pop rbx
			pop r15
			pop r14
			pop r13
			pop r12
			pop	rbp
			ret

	; void descifrarMensajeDiabolico( lista *l, char *archivo, void (*funcImpPbr)(char*,FILE* ) ) {
	;     FILE *file = fopen(archivo,"a");
	;     if (l->primero == NULL) {
	;         fprintf(file, "%s\n", "<sinMensajeDiabolico>");        
	;     } else {    
	;         descifrarMensajeDiabolicoRecursivo(l->primero, file, funcImpPbr);
	;     }
	;     fclose(file);
	; }
	descifrarMensajeDiabolico:
		;STACKFRAME
						;DESALINEADA
		push rbp		;ALINEADA
		mov rbp, rsp
		push r12		;DESALINEADA
		push r13		;ALINEADA
		push r14		;DESALINEADA
		push r15		;ALINEADA
		
		;CODIGO

		; rdi = l
		; rsi = archivo
		; rdx = funcImpPbr

		;salvo parametros
		mov r12, rdi
		mov r13, rsi
		mov r14, rdx

		;FILE *file = fopen(archivo,"a");
		mov rdi, r13
		mov rsi, append
					; ALINEADA
		call fopen
		mov r15, rax
		; r15 : direccion del archivo abierto

		; if (l->primero == NULL) {
		;     fprintf(file, "%s\n", "<sinMensajeDiabolico>");        
		; } else {    
		;     descifrarMensajeDiabolicoRecursivo(l->primero, file, funcImpPbr);
		; }
		cmp qword [r12 + OFFSET_PRIMERO], NULL
		je .sin_mensaje_diabolico
			mov rdi, [r12 + OFFSET_PRIMERO]
			mov rsi, r15
			mov rdx, r14
			call descifrarMensajeDiabolicoRecursivo

			jmp .fin

		.sin_mensaje_diabolico:
		; fprintf(file, "%s\n", "<sinMensajeDiabolico>");
		mov rdi, r15					; rdi = file
		mov rsi, format_string_con_LF
		mov rdx, sinMensajeDiabolico	;para imprimir '<sinMensajeDiabolico>'
		mov rax, 1 						;para llamar a fprintf
		call fprintf					;ALINEADA
		jmp .fin


		.fin:
			; fclose(file)
			mov rdi, r15
			call fclose
			;FIN
			pop r15
			pop r14
			pop r13
			pop r12
			pop	rbp
			ret


	; void descifrarMensajeDiabolicoRecursivo( nodo* it, FILE *file, void (*funcImpPbr)(char*,FILE* ) ) {
	;     if (it->siguiente != NULL) { // recorro hasta el último elemento
	;         descifrarMensajeDiabolicoRecursivo(it->siguiente, file, funcImpPbr);
	;     } 
	;     funcImpPbr(it->palabra, file);
	; }
	descifrarMensajeDiabolicoRecursivo:
		;STACKFRAME
						;DESALINEADA
		push rbp		;ALINEADA
		mov rbp, rsp
		push r12		;DESALINEADA
		push r13		;ALINEADA
		push r14		;DESALINEADA
		sub rsp, 8		;ALINEADA

		;CODIGO
		; rdi = it
		; rsi = file
		; rdx = funcImpPbr

		;salvo parametros
		mov r12, rdi
		mov r13, rsi
		mov r14, rdx
		
		; if (it->siguiente != NULL) { // recorro hasta el último elemento
		;     descifrarMensajeDiabolicoRecursivo(it->siguiente, file, funcImpPbr);
		; } 
		cmp qword [r12 + OFFSET_SIGUIENTE], NULL
		je .fin_recorrer_lista_recursivamente
			mov rdi, [r12 + OFFSET_SIGUIENTE]
			mov rsi, r13
			mov rdx, r14
			call descifrarMensajeDiabolicoRecursivo

		.fin_recorrer_lista_recursivamente:
		mov rdi, [r12 + OFFSET_PALABRA]
		mov rsi, r13
		call r14

		.fin:
			;FIN
			add rsp, 8
			pop r14
			pop r13
			pop r12
			pop	rbp
			ret		